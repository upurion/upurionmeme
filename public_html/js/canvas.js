/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var canvas = document.querySelector("#c");
var ctx = canvas.getContext("2d");
var img;
var imgData;
var imgRotation = 0; // Rotation clockwise by 90°
var imageLoader = document.getElementById('imageLoader');
var topInput = document.getElementById('topText');
var topColorInput = document.getElementById('topColor');
var bottomInput1 = document.getElementById('bottomText1');
var bottomInput2 = document.getElementById('bottomText2');
var bottomColorInput = document.getElementById('bottomColor');
var effectBWCheckBox = document.getElementById('effectBW');
var effectICCheckBox = document.getElementById('effectIC');

imageLoader.addEventListener('change', handleImage, false);

// Reload the meme at any change
topInput.addEventListener('input', changeMeme, false);
// topColorInput.addEventListener('change', changeMeme, false);
bottomInput1.addEventListener('input', changeMeme, false);
bottomInput2.addEventListener('input', changeMeme, false);
// bottomColorInput.addEventListener('change', changeMeme, false);
effectBWCheckBox.addEventListener('change', function () {
    changeMeme(true);
}, false);
effectICCheckBox.addEventListener('change', function () {
    changeMeme(true);
}, false);


function rotateImg() {
    if (imgRotation === 3)
    {
        imgRotation = 0;
    } else {
        imgRotation++;
    }
    changeMeme(true);
}

// When an image is loaded set img variable and redraw the meme
function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        img = new Image();
        img.onload = function () {
            canvas.width = 500;
            canvas.height = 500;
            changeMeme(true)
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

// Redraw the meme
function changeMeme(reloadImg) {
    var topText = topInput.value || "";
    var topColor = topColorInput.value;
    var bottomText1 = bottomInput1.value || "";
    var bottomText2 = bottomInput2.value || "";
    var bottomColor = bottomColorInput.value;
    // Clear the canvas
    c.width = c.width;
    // Insert image
    if (img != null) {
        // Don't reload the img and apply the effect if not needed
        if (reloadImg === true) {
            ctx.save();
            if (imgRotation === 1) {
                ctx.translate(c.width, 0)
            }
            ;

            if (imgRotation === 2) {
                ctx.translate(c.width, c.height)
            }
            ;
            if (imgRotation === 3) {
                ctx.translate(0, c.height)
            }
            ;
            ctx.rotate(imgRotation * Math.PI / 2);
            ctx.drawImage(img, 0, 0, c.width, c.height);
            imgEffects();
            ctx.restore();
        } else {
            ctx.putImageData(imgData, 0, 0);
        }
    }
    ;
    // Default font
    ctx.font = "36pt Impact ";
    ctx.lineWidth = 3;
    ctx.textAlign = "center"

    // Write top text 
    ctx.fillStyle = topColor;
    ctx.fillText(topText, c.width / 2, 50);
    ctx.strokeText(topText, c.width / 2, 50);

    // Write bottom text 
    ctx.fillStyle = bottomColor;
    ctx.fillText(bottomText1, c.width / 2, 400);
    ctx.strokeText(bottomText1, c.width / 2, 400);
    ctx.fillText(bottomText2, c.width / 2, 450);
    ctx.strokeText(bottomText2, c.width / 2, 450);
}


function imgEffects() {
    imgData = ctx.getImageData(0, 0, c.width, c.height);
    if (effectICCheckBox.checked === true) {
        imgICEffect(imgData);
    }
    if (effectBWCheckBox.checked === true) {
        imgBWEffect(imgData);
    }
    ctx.putImageData(imgData, 0, 0);
}

// Inverse color effect
function imgICEffect(imgData) {
    for (var i = 0; i < (c.width * c.height); i += 1) {
        imgData.data[(i * 4) + 0] = 255 - imgData.data[(i * 4) + 0];
        imgData.data[(i * 4) + 1] = 255 - imgData.data[(i * 4) + 1];
        imgData.data[(i * 4) + 2] = 255 - imgData.data[(i * 4) + 2];
    }
}

// Black and White effect
function imgBWEffect(imgData) {
    for (var i = 0; i < (c.width * c.height); i += 1) {
        // Formula can be found on the web to have a better effect
        var average = (imgData.data[(i * 4) + 0] + imgData.data[(i * 4) + 1] + imgData.data[(i * 4) + 2]) / 3;
        imgData.data[(i * 4) + 0] = average;
        imgData.data[(i * 4) + 1] = average;
        imgData.data[(i * 4) + 2] = average;
        imgData.data[(i * 4) + 3] = 255;
    }
}